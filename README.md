# GenEx

[![Crates.io](https://img.shields.io/crates/v/genex.svg)](https://crates.io/crates/genex)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Documentation](https://docs.rs/genex/badge.svg)](https://docs.rs/genex)
[![Coverage](https://gitlab.com/jjwiseman/genex/badges/master/coverage.svg)]()

GenEx is a text template expansion library.
