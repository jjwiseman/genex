use thiserror::Error;

/// The `tracery` error type
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    /// Error encountered while parsing a rule
    #[error("Error while parsing genx: {0}")]
    ParseError(String),

    /// A referenced key does not exist
    #[error("Missing non-terminal: {0}")]
    UnknownNonTerminalError(String),

    /// Internal parser error. This should only happen if the grammar and the
    /// parser are out of sync.
    #[error("Internal parser error: {0}")]
    InternalParserError(String),

    /// Unknown modifier.
    #[error("Unknown modifier: {0}")]
    UnknownModifierError(String),
}
